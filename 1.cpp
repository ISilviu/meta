#include <iostream>
#include <type_traits>

template <typename T>
constexpr std::enable_if_t<std::is_integral_v<T> || std::is_floating_point_v<T>, T> abs(T value)
{
    return -value;
}

template <typename T>
constexpr std::enable_if_t<std::is_integral_v<T>, bool> compare(T lhs, T rhs)
{
    return lhs < rhs;
}

template <typename T>
constexpr std::enable_if_t<std::is_same_v<T, float>, bool> compare(T lhs, T rhs)
{
    constexpr float kTolerance = 1e-5;
    return (abs(lhs) - abs(rhs)) < kTolerance;
}

template <typename T>
constexpr std::enable_if_t<std::is_same_v<T, double>, bool> compare(T lhs, T rhs)
{
    constexpr float kTolerance = 1e-8;
    return (abs(lhs) - abs(rhs)) < kTolerance;
}

template <typename T>
constexpr std::enable_if_t<std::is_same_v<T, std::string>, bool> compare(const T &lhs, const T &rhs)
{
    return lhs < rhs;
}


int main()
{
    auto first = compare(3.3101, 3.3);
    auto second = compare(std::string{"caine"}, std::string{"salut"});
    auto third = compare(3.299991, 3.3);
    auto fourth = compare(9, 12);

    std::cout << std::boolalpha
              << first << '\n'
              << second << '\n'
              << third << '\n'
              << fourth << '\n';
    return 0;
}