#include <iostream>
#include <type_traits>


struct Func
{
    double Function(int x, int z)
    {
        return 2.20;
    }

    double operator()(int x, int y)
    {
        return 5.5;
    }

};

double Fn(int y)
{
    return 24.53;
}



int main()
{
    std::result_of_t<Func(int, int)> x = 5.4;
    std::result_of_t<decltype(&Fn)(int)> y = 2.5;

    std::cout<< x << '\n' << y << '\n';
    return 0;
}