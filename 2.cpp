#include <iostream>
#include <type_traits>
#include <string>

namespace isilviu
{   
    template <typename T, typename ReturnType>
    struct has_function
    {
        template <bool  = false>
        static constexpr bool get() 
        {
            return false;
        }

        static constexpr std::enable_if_t<std::is_same_v<ReturnType(T::*)(), decltype(&T::print)>, bool> get() 
        {
            return true;
        }
    };

    template <typename T, typename ReturnType>
    inline constexpr auto has_print = has_function<T, ReturnType>::get();
}

namespace test
{
    class Foo
    {
    public:
        void print();
    };

    class SomeFoo
    {
    public:
        int print() { return 1; }
    };

    class SomeOtherFoo
    {
    public:
        void doSomething() {}
    };
}

int main()
{
    auto hasPrint = isilviu::has_print<test::SomeFoo, void>;
    std::cout << hasPrint << '\n';
    return 0;
}